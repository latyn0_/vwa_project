from flask import Flask, redirect, url_for, render_template, request, session, flash, Blueprint
from werkzeug.security import generate_password_hash
from catalog.catalog import categories
from database.db import db, users, category, product

user = Blueprint("user", __name__, static_folder="static", template_folder="templates")

#user page
@user.route("/", methods=["POST", "GET"])
def userpage():
	email = None
	if "user" in session:
		user = session["user"]

		if request.method == "POST":
			email = request.form["email"]
			old_pass = request.form["old_pass"]
			new_pass = request.form["new_pass"]
			found_user = users.query.filter_by(name=user).first()
			#nove heslo
			if len(new_pass) > 0:
				#overeni stareho hesla a zadani noveho
				if found_user.verify_password(old_pass):
					found_user.password_hash = generate_password_hash(new_pass)
					db.session.commit()
					flash("Provedení změn úspěšné")
				else:
					flash("Zadali jste špatné heslo")
			session["email"] = email
			found_user.email = email
			db.session.commit()
			flash("Provedení změn úspešné")
		else:
			found_user = users.query.filter_by(name=user).first()
			email = found_user.email

		return render_template("user.html", email=email)
	else:
		flash("Nejste přihlášeni", "info")
		return redirect(url_for("verification.login"))
		
#pridani kategorie
@user.route("/newcategory", methods=["POST", "GET"])
def category_add():
	if "user" in session:
		if request.method == "POST":
			name = request.form["name"]
			desc = request.form["desc"]
			cat = category(name, desc)
			db.session.add(cat)
			db.session.commit()
			flash("Kategorie byla úspěšně vytvořena")   
			return redirect(url_for("user.category_add"))

		return render_template("new_category.html")
	else:
		flash("Nejste přihlášeni", "info")
		return redirect(url_for("verification.login"))

#odebrani kategorie
@user.route("/remove-category", methods=["POST", "GET"])
def category_remove():
	if "user" in session:
		all_catgrs = category.query.all()
		if request.method == "POST":
			catgr = request.form["category"]
			found_catgr = category.query.filter_by(name=catgr).first()
			db.session.delete(found_catgr)
			db.session.commit()
			flash("Kategorie odebrána")   
			return redirect(url_for("user.category_remove"))

		return render_template("remove_category.html", categories=all_catgrs)
	else:
		flash("Nejste přihlášeni", "info")
		return redirect(url_for("verification.login"))

#pridani produktu
@user.route("/newproduct", methods=["POST", "GET"])
def product_add():
	if "user" in session:
		if request.method == "POST":
			name = request.form["name"]
			manufacturer = request.form["manufacturer"]
			catgr = request.form["category"]
			selected_catgr = category.query.filter_by(name=catgr).first()
			cat_id = selected_catgr.id
			new_product = product(name, manufacturer, cat_id)
			db.session.add(new_product)
			db.session.commit()
			flash("Produkt byl úspěšně vytvořen") 
			return redirect(url_for("user.product_add"))
			
		categories = category.query.all()
		return render_template("new_product.html", categories=categories)
	else:
		flash("Nejste přihlášeni", "info")
		return redirect(url_for("verification.login"))

#odebrani kategorie
@user.route("/remove-product", methods=["POST", "GET"])
def product_remove():
	if "user" in session:
		all_catgrs = category.query.all()
		all_products = product.query.all()
		if request.method == "POST":
			_product = request.form["product"]
			found_product = product.query.filter_by(name=_product).first()
			db.session.delete(found_product)
			db.session.commit()
			flash("Produkt odebrán")   
			return redirect(url_for("user.product_remove"))

		return render_template("remove-product.html", categories=all_catgrs, products=all_products)
	else:
		flash("Nejste přihlášeni", "info")
		return redirect(url_for("verification.login"))
