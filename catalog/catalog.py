from flask import Flask, redirect, url_for, render_template, request, session, flash, Blueprint
from database.db import db, category, product, prices
from datetime import datetime

catalog = Blueprint("catalog", __name__, static_folder="static", template_folder="templates")

@catalog.route("/")
def categories():
    categories = category.query.all()
    return render_template("categories.html", categories=categories)

@catalog.route("/category=<cat>")
def category_page(cat):
    x = cat.lower()
    found_category = category.query.filter_by(lowercase_name=x).first()
    if found_category == None:
        return '404'
    else:
        products = product.query.filter_by(category_id=found_category.id).all()
        return render_template("category.html", category=found_category, products=products)


@catalog.route("/category=<cat>/product=<product_name>", methods=["POST", "GET"])
def product_page(cat, product_name):
    x = product_name.lower()
    catgr = cat.lower()
    found_category = category.query.filter_by(lowercase_name=catgr).first()
    found_product = product.query.filter_by(category_id=found_category.id, lowercase_name=x).first()
    if found_product == None:
        return '404'
    else:
        if request.method == "POST":
            d = request.form["date"]
            date = datetime.strptime(d, '%Y-%m-%d')
            price = request.form["price"]
            source = request.form["source"]
            if len(source) == 0:
                source = "/"
            new_price = prices(found_product.id, price, date, source)
            db.session.add(new_price)
            db.session.commit()
        found_prices = prices.query.filter_by(product=found_product.id).all()
        max_price = prices.query.filter_by(product=found_product.id).order_by(prices.price.desc()).first()
        min_price = prices.query.filter_by(product=found_product.id).order_by(prices.price).first()
        sum = 0
        i = 0
        for price in found_prices:
            sum += price.price
            i+=1
        if i>0:
            avg = round(sum/i, 1)
        else:
            avg=0
        days = []
        months = []
        years = []
        _prices = []
        i = 0
        ordered_prices = prices.query.filter_by(product=found_product.id).order_by(prices.date).all()
        for price in ordered_prices:
            days.append(price.date.strftime('%d'))
            months.append(price.date.strftime('%m'))            
            years.append(price.date.strftime('%Y'))
            _prices.append(price.price)
            i = i + 1
        return render_template("product.html", product=found_product, number=i, m=months, d=days, y=years, _prices=_prices, prices=found_prices, max = max_price, min = min_price, avg=avg, category=found_category)


@catalog.route("/delete/<product_id>/<price_id>")
def delete_price(product_id, price_id):
    #mazani ceny
    price = prices.query.filter_by(id=price_id).first()
    db.session.delete(price)
    db.session.commit()
    #zjistovani parametru
    found_product = product.query.filter_by(id=product_id).first()
    found_category = category.query.filter_by(id=found_product.category_id).first()
    found_prices = prices.query.filter_by(product=found_product.id).all()
    flash("Cena smazána")
    return redirect(url_for("catalog.product_page", cat=found_category.name, product_name=found_product.name))