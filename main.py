from flask import Flask, redirect, url_for, render_template, request, session, flash
from datetime import timedelta
from verification.verification import verification
from user.user import user
from catalog.catalog import catalog
from database.db import db

app = Flask(__name__)
app.register_blueprint(verification, url_prefix="")
app.register_blueprint(user, url_prefix="/user")
app.register_blueprint(catalog, url_prefix="")

app.secret_key = "hello"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/db.sqlite3'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.permanent_session_lifetime = timedelta(minutes=60)


db.app = app
db.init_app(app)


@app.route("/")
def index():
	return redirect(url_for('catalog.categories'))


if __name__ == "__main__":
	db.create_all()
	app.run(debug = True)