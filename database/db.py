from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from sqlalchemy.orm.base import DEFAULT_STATE_ATTR
from werkzeug.security import check_password_hash, generate_password_hash

db = SQLAlchemy()

#tabulka users
class users(db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100))
	password_hash = db.Column(db.String(128))
	email = db.Column(db.String(100))

	def __init__(self, name, password, email):
		self.name = name
		self.password_hash = generate_password_hash(password)
		self.email = email

	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)

	def hash_password(self, password):
		return generate_password_hash(password)

#tabulka kategorie
class category(db.Model):
	__tablename__ = 'category'
	id = db.Column("id", db.Integer, primary_key=True)
	name = db.Column("name", db.String(100))
	lowercase_name = db.Column("lc_name", db.String(100))
	description = db.Column("description", db.String(200))
	children = relationship("product")

	def __init__(self, name, description):
		self.name = name
		self.lowercase_name = name.lower()
		self.description = description

#tabulka produkt
class product(db.Model):
	__tablename__ = 'product'
	id = db.Column("id", db.Integer, primary_key=True)
	name = db.Column("name", db.String(100))
	lowercase_name = db.Column("lc_name", db.String(100))
	manufacturer = db.Column("manufacturer", db.String(100))
	category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
	children = relationship("prices")

	def __init__(self, name, manufacturer, category_id):
		self.name = name
		self.lowercase_name = name.lower()
		self.manufacturer = manufacturer
		self.category_id = category_id

#tabulka cen
class prices(db.Model):
	__tablename__ = 'prices'
	id = db.Column(db.Integer, primary_key=True)
	product = db.Column(db.Integer, db.ForeignKey('product.id'))
	price = db.Column(db.Float)
	date = db.Column(db.Date)
	comment = db.Column(db.String(200))

	def __init__(self, product, price, date, comment):
		self.product = product
		self.price = price
		self.date = date
		self.comment = comment