from flask import Flask, redirect, url_for, render_template, request, session, flash, Blueprint
from datetime import timedelta
from database.db import db, users


verification = Blueprint("verification", __name__, static_folder="static", template_folder="templates")


#přihlášení
@verification.route("/login", methods=["POST", "GET"])
def login():
	if request.method == "POST":
		user = request.form["nm"]
		password = request.form["pwd"]

		#overeni uzivatele
		found_user = users.query.filter_by(name=user).first()
		if found_user == None:
			flash("Špatné přihlašovací údaje")
			return redirect(url_for("verification.login"))
		else:
			if found_user.verify_password(password):
				session["user"] = user
				session["password"] = password
				session.permanent = True				
				flash("Přihlášení úspěšné!")
				return redirect(url_for("user.userpage"))
			else:
				flash("Špatné přihlašovací údaje")
				return redirect(url_for("verification.login"))
			
		
	else:
		if "user" in session:
			flash("Jste už přihlášeni")
			return redirect(url_for("user.userpage"))
		return render_template("login.html")


#odhlášení
@verification.route("/logout")
def logout():
	session.pop("user", None)
	session.pop("password", None)
	session.pop("email", None)
	flash("Byli jste odhlášeni!")
	return redirect(url_for("verification.login"))



#registrování
@verification.route("/register", methods=["POST", "GET"])
def register():
	if request.method == "POST":
		user = request.form["nm"]
		password = request.form["pwd"]
		email = request.form["email"]
		found_user = users.query.filter_by(name=user).first()
		found_email = users.query.filter_by(email=email).first()

	else:
		return render_template("register.html")

	if found_user:
		flash("Toto jméno už existuje")
		return render_template("register.html")
	else:
		if found_email:
			flash("Tento e-mail už existuje")
			return render_template("register.html")

		else:
			usr = users(user, password, email)
			db.session.add(usr)
			db.session.commit()
			flash("Úspěšně jste vytvořili nový účet")
			return redirect(url_for("verification.login"))